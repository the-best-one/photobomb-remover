# Yet Another Photobomb Remover

This software detects people/dogs/cats on a photo and lets users remove selected one from the photo.

## Installation

Windows
```bash
install.bat
```
Linux
```bash
install.sh
```

## Usage

```bash
flask --app flaskapp.py run
```
## I just want to push "Go" button
Well, I've got you covered! There is a colab ready to go
https://colab.research.google.com/drive/1kRnPexlzai9KI3akCxUWE0_Yw77hvJgi#scrollTo=3wxcjEonp-lj
