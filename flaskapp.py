import os
import uuid
import redis
from rq import Queue
from flask import Flask
from flask import render_template, request, redirect, send_from_directory
from werkzeug.utils import secure_filename
from instance_segmentation import Segmenter
from inpainting import Inpainter
from redis_queue import conn
import threading
from pyngrok import ngrok


def init_flask():
    """
    init_flask
    method:
      - initializes flask application
      - sets all variables
    """
    ap = Flask(__name__)
    ap.config['MAX_CONTENT_LENGTH'] = 500 * 1024
    ap.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif']
    ap.config['UPLOAD_PATH'] = 'uploads'
    ap.config['ASSETS_PATH'] = 'assets'
    ap.config['INPAINTING_PATH'] = os.path.join('assets', 'inpainting')
    ap.config['DEMO_PATH'] = os.path.join('assets',
                                          'inpainting',
                                          'demonstration')
    ap.config['INPAINTING_INPUT_PATH'] = os.path.join(os.getcwd(),
                                                      'assets',
                                                      'inpainting',
                                                      'data_for_prediction')
    ap.config['INPAINTING_OUTPUT_PATH'] = os.path.join(os.getcwd(),
                                                       'assets',
                                                       'inpainting',
                                                       'output')
    ap.config['REDIS_URL'] = 'redis://localhost:6379'

    ap.redis = redis.from_url(ap.config['REDIS_URL'])
    ap.queue = Queue(connection=conn)
    ap.segmenter = Segmenter(ap.config['DEMO_PATH'],
                             ap.config['INPAINTING_PATH'],
                             ap.config['ASSETS_PATH'],
                             ap.config['REDIS_URL'])
    ap.inpainter = Inpainter(ap.config['INPAINTING_INPUT_PATH'],
                             ap.config['INPAINTING_OUTPUT_PATH'])
    ap.redis.set('current_inpainting_job', 123)

    port = 5000
    public_url = ngrok.connect(port).public_url
    print(f' * ngrok tunnel "{public_url}" -> "http://127.0.0.1:{port}"')
    ap.config["BASE_URL"] = public_url
    return ap


app = init_flask()


@app.errorhandler(413)
def too_large(e):
    """
    init_flask
    parameters:
      - e - error
    method:
      - processes error of too big uploaded file
    returns:
      - 413 status
    """
    return "File is too large", 413


@app.route('/')
def index():
    """
    index
    method:
      - processes index page
    returns:
      - index page
    """
    return render_template('index.html')


@app.route('/', methods=['POST'])
def upload_files():
    """
    upload_files
    method:
      - processes file upload
      - forwards to image segmentation
    returns:
      - forward to next step
    """
    uploaded_file = request.files['file']
    filename = secure_filename(uploaded_file.filename)
    file_ext = os.path.splitext(uploaded_file.filename)[1]
    if filename != '':
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            return "Invalid image", 400
        filename = str(uuid.uuid4()) + file_ext
        uploaded_file.save(os.path.join(app.config['UPLOAD_PATH'], filename))
        return redirect(f'/segment/{filename}', 302)
    return '', 204


@app.route('/segment/<filename>')
def segment(filename):
    """
    segment
    parameters:
      - filename - name of file for segmentation
    method:
      - moves file to appropriate directory
      - forwards to next step
    returns:
      - segmentation queue page
    """
    src_filename = os.path.join(app.config['UPLOAD_PATH'], filename)
    dst_filename = os.path.join(app.config['ASSETS_PATH'], filename)
    app.redis.hset('dst', filename, dst_filename)
    os.rename(src_filename, dst_filename)
    return redirect(f'/await_segmentation/{filename}', 302)


@app.route('/await_segmentation/<filename>')
def await_segmentation(filename):
    """
    await_segmentation
    parameters:
      - filename - name of file for segmentation
    method:
      - starts segmentation job
      - shows segmentation queue
      - shows result
    returns:
      - forward to next step
    """
    finished = False
    wait = 1
    queue = 'being estimated'
    if app.redis.hexists('segment', filename):
        wait = 5
        job = app.queue.fetch_job(filename)
        if job:
            if job.is_finished:
                demo_path = app.redis.hget('demos', filename).decode()
                bombs = (app.redis.hget('bombs', filename)).decode().split(',')
                app.redis.hdel('segment', filename)
                app.redis.hdel('demos', filename)
                app.redis.hdel('bombs', filename)
                finished = True
            else:
                queue = app.queue.get_job_position(job)

        else:
            dst_filename = app.redis.hget('dst', filename)
            job = app.queue.enqueue_call(func=app.segmenter.segment_instance,
                                         job_id=filename,
                                         args=(dst_filename, filename),
                                         result_ttl=5000)
            queue = app.queue.get_job_position(job)
    else:
        app.redis.hset('segment', filename, 1)
    if finished:
        return render_template('segmentation.html',
                               filename=os.path.split(demo_path)[-1],
                               bombs=bombs)
    else:
        return render_template('await_segmentation.html',
                               wait=wait,
                               queue=queue)


@app.route('/start_inpainting', methods=['POST'])
def start_inpainting():
    """
    start_inpainting
    method:
      - moves files to appropriate directory
      - gets data for inpainting
      - forwards to next step
    returns:
      - inpainting queue page
    """
    filename = request.form.get('filename')
    bomb = request.form.get('bomb')
    move_segmented_files(filename, bomb)
    return redirect(f'/await_inpainting/{filename}/{bomb}', 302)


@app.route('/await_inpainting/<filename>/<bomb>')
def await_inpainting(filename, bomb):
    """
    await_inpainting
    parameters:
      - filename - name of file for inpainting
      - bomb - object for removal index
    method:
      - starts inpainting job
      - shows inpainting queue
      - shows result
    returns:
      - forward to next step
    """
    finished = False
    wait = 1
    queue = 'being estimated'
    if app.redis.hexists('inpaint', filename):
        wait = 5
        inpainting_job_id = app.redis.hget('inpainting_job', filename)
        if inpainting_job_id:
            inpainting_job_id = inpainting_job_id.decode()
            job = app.queue.fetch_job(inpainting_job_id)
            if job:
                if job.is_finished:
                    app.redis.hdel('inpaint', filename)
                    app.redis.hdel('inpainting_job', filename)
                    finished = True
                else:
                    queue = app.queue.get_job_position(job)
        else:
            current_job = app.redis.get('current_inpainting_job').decode()
            job = app.queue.fetch_job(current_job)
            start_new = True
            if job:
                position = app.queue.get_job_position(job)
                if position:
                    if position > 0:
                        app.redis.hset('inpainting_job',
                                       filename,
                                       current_job)
                        start_new = False

            if start_new:
                current_job = str(uuid.uuid4())
                app.queue.enqueue_call(func=app.inpainter.inpaint,
                                       job_id=current_job,
                                       result_ttl=5000)
                app.redis.set('current_inpainting_job', current_job)
                app.redis.hset('inpainting_job', filename, current_job)

            queue = app.queue.get_job_position(current_job)
    else:
        app.redis.hset('inpaint', filename, 1)

    if finished:
        return redirect(f'/inpaint/{filename}/{bomb}', 302)
    else:
        return render_template('await_inpainting.html', wait=wait, queue=queue)


@app.route('/inpaint/<filename>/<bomb>')
def inpaint(filename, bomb):
    """
    inpaint
    parameters:
      - filename - name of file for inpainting
      - bomb - object for removal index
    method:
      - redirects to result
    returns:
      - forward to result
    """
    return redirect(f'/results/{filename}', 302)


@app.route('/results/<filename>')
def result(filename):
    """
    result
    parameters:
      - filename - name of file for inpainting
    method:
      - redirects to result
    returns:
      - result page
    """
    return render_template('result.html', filename=filename)


@app.route('/uploads/<filename>')
def upload(filename):
    """
    upload
    parameters:
      - filename - name of file for inpainting
    method:
      - uploads inpainted file
    returns:
      - inpainted file
    """
    return send_from_directory(app.config['INPAINTING_OUTPUT_PATH'],
                               f'{os.path.splitext(filename)[0]}_mask.png')


@app.route('/demos/<filename>')
def demos(filename):
    """
    demos
    parameters:
      - filename - name of file for segmentation
    method:
      - uploads segmented file
    returns:
      - segmented file
    """
    return send_from_directory(app.config['DEMO_PATH'], filename)


@app.route('/favicon.ico')
def favicon():
    """
    favicon
    method:
      - uploads favicon.ico file
    returns:
      - favicon.ico file
    """
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


def move_segmented_files(filename, bomb):
    """
    move_segmented_files
    parameters:
      - filename - name of file for inpainting
      - bomb - object for removal index
    method:
      - moves files for inpainting to appropriate directory
    """
    no_ext_name = os.path.splitext(filename)[0]
    base_src_path = os.path.join(app.config['INPAINTING_PATH'], no_ext_name)
    base_dst_path = os.path.join(app.config['INPAINTING_INPUT_PATH'],
                                 no_ext_name)
    bomb_path = f'{base_src_path}-{bomb}.png'
    mask_path = f'{base_dst_path}_mask.png'
    os.rename(bomb_path, mask_path)

    file_path = os.path.join(app.config['ASSETS_PATH'], filename)
    os.rename(file_path, f'{base_dst_path}.png')


os.environ.pop("FLASK_RUN_FROM_CLI")
threading.Thread(target=app.run, kwargs={"use_reloader": False}).start()
