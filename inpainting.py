import os
import subprocess


class Inpainter():
    def __init__(self, input_path, output_path):
        """
        __init__
        parameters:
          - input_path - directory with files to process
          - output_path - directory to save result files
        method:
          - initializes parameters for inpainting
        """
        self.is_linux = os.name == ('posix')
        self.input_path = input_path
        self.output_path = output_path
        self.model_path = os.path.join(os.getcwd(), 'lama', 'big-lama')
        self.cwd = os.path.join(os.getcwd(), 'lama') \
            if self.is_linux \
            else os.path.join(os.getcwd(), 'lama', 'bin')
        self.script_path = 'predict.py'

    def inpaint(self):
        """
        inpaint
        method:
          - runs LaMa inpainting
          - removes processed files
        """
        files = os.listdir(self.input_path)
        if self.is_linux:
            os.system(f'PYTHONPATH={self.cwd} TORCH_HOME=$(pwd) python3 '
                      f'{self.cwd}/bin/predict.py model.path={self.model_path}'
                      f' indir={self.input_path} outdir={self.output_path}')
        else:
            subprocess.run(['python3', f'{self.script_path}',
                            f'model.path={self.model_path}',
                            f'indir={self.input_path}',
                            f'outdir={self.output_path}'], cwd=self.cwd)
        for file in [os.path.join(self.input_path, x) for x in files]:
            os.remove(file)
