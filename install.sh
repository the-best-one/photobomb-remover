git clone https://github.com/saic-mdal/lama.git
pip install -r requirements.txt
pip install -r lama/requirements.txt --quiet
pip install wget --quiet
pip install torchtext==0.9
python3 -m pip install --upgrade pip install torch==1.8.0+cu111 torchvision==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html
pip install wldhx.yadisk-direct


cd lama
curl -L $(yadisk-direct https://disk.yandex.ru/d/ouP6l8VJ0HpMZg) -o big-lama.zip
unzip big-lama.zip

pip uninstall opencv-python-headless -y --quiet
pip install opencv-python-headless==4.1.2.30 --quiet

cd ..
mkdir ./uploads
mkdir ./assets/inpainting
mkdir ./assets/inpainting/data_for_prediction
mkdir ./assets/inpainting/demonstration
mkdir ./assets/inpainting/output