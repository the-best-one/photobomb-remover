from PIL import Image
import torchvision.transforms as transforms
from torchvision.models.detection import maskrcnn_resnet50_fpn
import numpy as np
import redis
import cv2
import random
import warnings
import os


class Segmenter():
    def __init__(self, demo_path, inpainting_path, assets_path, redis_url):
        """
        __init__
        parameters:
          - demo_path - directory to save demonstration files to
          - inpainting_path - directory to save files for inpainting to
          - assets_path - directory to save images to
          - redis_url - url for redis server
        method:
          - colours the mask with random color
        returns:
          - coloured mask
          - color it was coloured with
        """
        warnings.filterwarnings('ignore')
        self.inpainting_path = inpainting_path
        self.demo_path = demo_path
        self.assets_path = assets_path
        self.redis_url = redis_url

        # load model
        self.model = maskrcnn_resnet50_fpn(pretrained=True)
        # set to evaluation mode
        self.model.eval()
        # load COCO category names
        self.COCO_CLASS_NAMES = [
            '__background__', 'person', 'bicycle', 'car', 'motorcycle',
            'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
            'fire hydrant', 'N/A', 'stop sign', 'parking meter', 'bench',
            'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
            'zebra', 'giraffe', 'N/A', 'backpack', 'umbrella', 'N/A', 'N/A',
            'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard',
            'sports ball', 'kite', 'baseball bat', 'baseball glove',
            'skateboard', 'surfboard', 'tennis racket', 'bottle', 'N/A',
            'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana',
            'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog',
            'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
            'N/A', 'dining table', 'N/A', 'N/A', 'toilet', 'N/A', 'tv',
            'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
            'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'N/A',
            'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
            'toothbrush'
        ]
        self.transform = transforms.Compose([transforms.ToTensor()])
        self.colours = [[0, 255, 0], [0, 0, 255], [255, 0, 0], [0, 255, 255],
                        [255, 255, 0], [255, 0, 255], [80, 70, 180],
                        [250, 80, 190], [245, 145, 50], [70, 150, 250],
                        [50, 190, 190]]

    def get_coloured_mask(self, mask):
        """
        get_coloured_mask
        parameters:
          - mask - predicted masks
        method:
          - colours the mask with random color
        returns:
          - coloured mask
          - color it was coloured with
        """
        r = np.zeros_like(mask).astype(np.uint8)
        g = np.zeros_like(mask).astype(np.uint8)
        b = np.zeros_like(mask).astype(np.uint8)
        colour = self.colours[random.randrange(0, 10)]
        r[mask == 1], g[mask == 1], b[mask == 1] = colour
        coloured_mask = np.stack([r, g, b], axis=2)
        return coloured_mask, colour

    def get_prediction(self, img_path, confidence=0.7):
        """
        get_prediction
        parameters:
          - img_path - path of the input image
          - confidence - threshold to keep the prediction or not
        method:
          - Image is obtained from the image path
          - the image is converted to image tensor using PyTorch's Transforms
          - image is passed through the model to get the predictions
          - masks, classes and bounding boxes are obtained from the model and
            soft masks are made binary(0 or 1) on masks
            ie: eg. segment of cat is made 1 and rest of the image is made 0
        returns:
          - masks
          - boxes for detected objects
          - predicted classes
        """
        img = Image.open(img_path)
        img = self.transform(img)
        prediction = self.model([img])
        predicted_score = list(prediction[0]['scores'].detach().numpy())
        predicted_confidence_border = [predicted_score.index(x) for x in
                                       predicted_score if x > confidence][-1]
        masks = prediction[0]['masks'] > 0.5
        masks = masks.squeeze().detach().cpu().numpy()
        predicted_class = [self.COCO_CLASS_NAMES[i]
                           for i in list(prediction[0]['labels'].numpy())]
        predicted_boxes = [[(i[0], i[1]), (i[2], i[3])] for i in
                           list(prediction[0]['boxes'].detach().numpy())]
        masks = masks[:predicted_confidence_border + 1]
        predicted_boxes = predicted_boxes[:predicted_confidence_border + 1]
        predicted_class = predicted_class[:predicted_confidence_border + 1]
        return masks, predicted_boxes, predicted_class

    def get_byte_row(self, row):
        """
        get_byte_row
        parameters:
          - row - numpy array with mask
        method:
          - sets white pixel if mask value is not 0
          - sets black pixel otherwise
        returns:
          - pixels for mask
        """
        return [[255, 255, 255] if x else [0, 0, 0] for x in row]

    def segment_instance(self, img_path, fkey):
        """
        segment_instance
        parameters:
          - img_path - path to input image
          - fkey - redis access key for processed file
        method:
          - prediction is obtained by get_prediction
          - each mask is given random color
          - each mask is added to the image in the ration 1:0.5 with opencv
          - final output is saved
        """
        confidence = 0.5
        rect_th = 1
        text_size = 1
        text_th = 2
        img_path = './' + str(img_path.decode())
        masks, boxes, predicted_class = self.get_prediction(img_path,
                                                            confidence)
        img = cv2.imread(img_path)
        img_id = os.path.split(img_path)[-1][:-4]
        cv2.imwrite(os.path.join(self.assets_path, f'{img_id}.png'), img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        objects = []
        for i in range(len(masks)):
            if predicted_class[i] in ('cat', 'dog', 'person'):
                rgb_mask, rgb = self.get_coloured_mask(masks[i])
                m = np.array([self.get_byte_row(x) for x in masks[i]])
                cv2.imwrite(os.path.join(self.inpainting_path,
                                         f'{img_id}-{i}.png'), m)
                img = cv2.addWeighted(img, 1, rgb_mask, 0.5, 0)
                pt1 = (int(boxes[i][0][0]), int(boxes[i][0][1]))
                pt2 = (int(boxes[i][1][0]), int(boxes[i][1][1]))
                cv2.rectangle(img=img, pt1=pt1, pt2=pt2, color=rgb,
                              thickness=rect_th)
                who = str(i)
                cv2.putText(img, who, pt1, cv2.FONT_HERSHEY_SIMPLEX,
                            text_size, rgb, thickness=text_th)
                objects.append(who)

        result_path = os.path.join(self.demo_path, f'{img_id}.png')
        cv2.imwrite(result_path, img)
        r = redis.from_url(self.redis_url)
        r.hset('demos', fkey, result_path)
        r.hset('bombs', fkey, ','.join(objects))
